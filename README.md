# Introduction
Allows the developer to create drush commands in an OOP manner.

# Use case
This is a plugin based way of creating drush commands.
There is no need to create a hook_drush_command().
All it takes is a plugin extending DrushCommandPluginBase and the proper 
annotation.

# Install
First you would need to install the module. In order to do this, you can either 
download the archive or use composer:
```composer require drupal/drush_command```

# Usage example
After installing the project, create a plugin as in this example:

``` PHP
namespace Drupal\custom_module\Plugin\DrushCommand
/**
 * Drush command example.
 *
 * This command can be ran in a crontab or triggered manually.
 *
 * @DrushCommand(
 *   id = "custom_drush_command",
 *   command = "custom-command",
 *   description = @Translation("Description of the custom drush command."),
 * )
 */
class CustomDrushCommand extends DrushCommandPluginBase {
  /**
   * What the command will actually do.
   */
  public function execute() {
    // The desired action will be executed here.
  }
}
```
This is the way to trigger the drush command:
```drush custom-command```
